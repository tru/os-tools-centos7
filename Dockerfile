FROM registry-gitlab.pasteur.fr/tru/docker-c7-ci:latest
MAINTAINER Tru Huynh <tru@pasteur.fr>

# install OS tools
RUN yum install -y make gcc gcc-c++ kernel-devel
RUN yum install -y pkgconfig
RUN yum install -y which
RUN yum install -y bzip2
RUN yum install -y epel-release
RUN yum -y upgrade
RUN yum install -y cmake3
RUN yum install -y environment-modules

# set environment variables
ENV LANG=C.UTF-8

# oneAPI repository
ARG repo=https://yum.repos.intel.com/oneapi
RUN echo -e "\
[oneAPI]\n\
name=Intel(R) oneAPI repository\n\
baseurl=${repo}\n\
enabled=1\n\
gpgcheck=1\n\
repo_gpgcheck=1\n\
gpgkey=https://yum.repos.intel.com/intel-gpg-keys/GPG-PUB-KEY-INTEL-SW-PRODUCTS-2023.PUB" \
> /etc/yum.repos.d/oneAPI.repo

